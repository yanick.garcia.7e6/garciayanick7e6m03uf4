﻿using System;

namespace Temporal
{
    /// <summary>
    /// ALUMNO: Yanick Garcia
    /// DESCRIPCION: Programa "temporal" para guardar, comparar y copiar fechas
    /// las cuales debido a pequeños problemas tecnicos en casa no ha podido ser
    /// entregado a tiempo.
    /// </summary>
    public class Data
    {
        public int dia { get; set; }
        public int mes { get; set; }
        public int anyo { get; set; }
        public Data(int dia, int mes, int anyo)
        {
            this.dia = dia;
            this.mes = mes;
            this.anyo = anyo;
        }
        public Data()
        {
            this.dia = 1;
            this.mes = 1;
            this.anyo = 1980;
        }
        public Data(Data data)
        {
            this.dia = data.getDia();
            this.mes = data.getMes();
            this.anyo = data.getAnyo();
        }
        public int getDia()
        {
            return dia;
        }
        public void getDia(int dia)
        {
            this.dia = dia;
        }
        public int getMes()
        {
            return mes;
        }
        public void getMes(int mes)
        {
            this.mes = mes;
        }
        public int getAnyo()
        {
            return anyo;
        }
        public void getAnyo(int anyo)
        {
            this.anyo = anyo;
        }
        public string dataString()
        {
            return $"{this.dia} / {this.mes} / {this.anyo}";
        }
        public Data sumDay(int diaSum)
        {
            this.dia += diaSum;
            if (this.dia > 30)
            {
                dia = dia - 30;
                mes++;
                if(mes > 12)
                {
                    mes = mes - 12;
                    anyo++;
                }
            }Data plusData = new Data(dia, mes, anyo);
            return plusData;
        }
        public Data minusDay(int diaMinus)
        {
            this.dia -= diaMinus;
            if (this.dia < 1)
            {
                dia = dia + 30;
                mes--;
                if (mes < 1)
                {
                    mes = mes + 12;
                    anyo--;
                }
            }
            Data minusData = new Data(dia, mes, anyo);
            return minusData;
        }
        public string compareData(Data dataComp)
        {
            bool major = true;
            if(this.dia > dataComp.dia)
            {
                major = true;
            }
            else
            {
                major = false;
            }
            if (this.mes > dataComp.mes)
            {
                major = true;
            }
            else
            {
                major = false;
            }
            if (this.anyo >= dataComp.anyo)
            {
                if(this.anyo == dataComp.anyo && this.mes == dataComp.mes && this.dia == dataComp.dia)
                {
                    return "Son la mateixa data";
                }
                major = true;
            }
            else
            {
                major = false;
            }
            if(major == true)
            {
                return "La teva data es major a la data amb que s'ha comparat";
            }
            else
            {
                return "La teva data es menor a la data amb que s'ha comparat";
            }
        }
        public static void Ordenar (IOrdenable[] obj)
        {
            for (int i = 0; i < array.Length; i++)
            {

            }
           if (obj == array[i])
            {

            }
        }
    }
    public interface IOrdenable
    {
        int Comparar(IOrdenable x);
        /* Retorna: 0 si this = x
        <0 si this < x
        >0 si this > x
        */

    }
    public class testing
    {
        static void Main(string[] args)
        {
            Data data1 = new Data();
            Console.WriteLine(data1.dataString());

            Data data2 = new Data(data1);
            Console.WriteLine(data2.dataString());

            Data data3 = new Data(13, 12, 2007);
            Console.WriteLine(data3.dataString());

            Data data4 = data3.sumDay(28);
            Console.WriteLine(data4.dataString());

            Data data5 = data3.minusDay(28);
            Console.WriteLine(data5.dataString());

            Console.WriteLine(data3.compareData(data1));
        }
    }

}
